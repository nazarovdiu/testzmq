package main

import (
	"consumer/consumer"
	"consumer/quitbysignal"
	"flag"
	"log"
)

func main() {
	log.Println("Begin consumer")
	endpoint := flag.String("uri",
		"tcp://127.0.0.1:5557",
		"URI to Producer",
	)
	dsn := flag.String(
		"dsn",
		"db/consumer.db",
		"DB DSN",
	)
	flag.Parse()
	ctx := quitbysignal.QuitBySignal()
	err := consumer.NewConsumer(ctx, *endpoint, *dsn).Run()
	log.Println("Stop; err = ", err)
}
