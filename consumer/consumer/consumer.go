package consumer

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/zeromq/goczmq"
	"log"
	"time"
)

// Message сообщение
type Message struct {
	Id  int    `json:"id"`
	Msg string `json:"msg"`
}

// Consumer  консумер
type Consumer struct {
	ctx      context.Context
	db       *sql.DB
	endpoint string
	socket   *goczmq.Sock
	isClosed bool
}

// NewConsumer -- конструктор консумера
func NewConsumer(ctx context.Context, endpoint, dsn string) *Consumer {
	return &Consumer{
		ctx:      ctx,
		db:       connectToDB(dsn),
		endpoint: endpoint,
		socket:   connectToZMQForPull(endpoint),
	}
}

// connectToDB -- это для подключения к БД
func connectToDB(dsn string) *sql.DB {
	db, err := sql.Open("sqlite3", dsn)
	if err != nil {
		log.Fatal("Can not connect to db")
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	return db
}

// connectToZMQForPull -- для подключения к поставщику данных
func connectToZMQForPull(endpoint string) *goczmq.Sock {
	socket, err := goczmq.NewPull("")
	if err != nil {
		log.Fatal(err)
	}
	err = socket.Connect(endpoint)
	if err != nil {
		log.Fatal(err)
	}
	return socket
}

// Close закрывает консумер
func (c *Consumer) Close() {
	err := c.db.Close()
	if err != nil {
		log.Println(err)
	}
	err = c.socket.Disconnect(c.endpoint)
	if err != nil {
		log.Println(err)
	}
	c.isClosed = true
	log.Println("Close")
}

// foreachMsg обрабатывает каждое сообщение
func (c *Consumer) foreachMsg(rcvMsg [][]byte) {
	message := &Message{}

	for _, v := range rcvMsg {
		err := json.Unmarshal(v, message)
		if err != nil {
			continue
		}
		_, err = c.db.Exec(
			"INSERT INTO consumer (ID, msg, dt) VALUES($1, $2, $3)",
			message.Id,
			message.Msg,
			time.Now().UTC().String(),
		)
		if err != nil {
			log.Println(err, message.Id, message.Msg)
		}
	}
}

// loopPull: в цикле получаем данные из очереди
func (c *Consumer) loopPull() {
	for {
		rcvMsg, _ := c.socket.RecvMessage()
		if rcvMsg == nil {
			time.Sleep(10 * time.Millisecond)
			continue
		}
		c.foreachMsg(rcvMsg)
	}
}

// Run -- запуск консумера
func (c *Consumer) Run() error {
	if c.isClosed {
		return fmt.Errorf("%s", "Consumer is closed")
	}
	defer c.Close()
	go c.loopPull()
	<-c.ctx.Done()
	return nil
}
