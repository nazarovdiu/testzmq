module producer

go 1.14

require (
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/zeromq/goczmq v4.1.0+incompatible
)
