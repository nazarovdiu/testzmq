package main

import (
	"flag"
	"log"
	"producer/producer"
	"producer/quitbysignal"
)

func main() {
	log.Println("Begin producer")
	endpoint := flag.String("uri",
		"tcp://127.0.0.1:5557",
		"URI for Producer",
	)
	dsn := flag.String("dsn",
		"db/producer.db",
		"DB DSN",
	)
	flag.Parse()
	ctx := quitbysignal.QuitBySignal()
	err := producer.NewProducer(ctx, *endpoint, *dsn).Run()
	log.Println("Stop; err = ", err)
}
