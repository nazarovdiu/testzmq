package quitbysignal

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
)

// QuitBySignal обработывает сигналы
func QuitBySignal() context.Context {
	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal)
	signal.Notify(sig, syscall.SIGQUIT, syscall.SIGINT)
	go func() {
		log.Println("SIGNAL:", <-sig)
		cancel()
	}()
	return ctx
}
