/*
	Поставщик данных
*/
package producer

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/zeromq/goczmq"
	"log"
	"time"
)

// Message -- структура сообщения
type Message struct {
	Id  int    `json:"id"`
	Msg string `json:"msg"`
	Dt  string `json:"dt"`
}

// Producer -- поставщик данных
type Producer struct {
	ctx      context.Context
	db       *sql.DB
	endpoint string
	socket   *goczmq.Sock
	isClosed bool
}

// NewProducer -- это конструктор поставищика данных
func NewProducer(ctx context.Context, endpoint, dsn string) *Producer {
	return &Producer{
		ctx:      ctx,
		db:       connectToDB(dsn),
		endpoint: endpoint,
		socket:   connectToZMQAsPush(endpoint),
	}
}

// connectToDB -- соединение с БД
func connectToDB(dsn string) *sql.DB {
	db, err := sql.Open("sqlite3", dsn)
	if err != nil {
		log.Fatal("Can not connect to db")
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	return db
}

// connectToZMQAsPush -- для создания Push в zmq
func connectToZMQAsPush(endpoint string) *goczmq.Sock {
	socket, err := goczmq.NewPush("")
	if err != nil || socket == nil {
		log.Fatal(err)
	}
	_, err = socket.Bind(endpoint)
	if err != nil {
		log.Fatal(err)
	}
	return socket
}

// Close закрывает поставщика данных
func (p *Producer) Close() {
	err := p.socket.Unbind(p.endpoint)
	if err != nil {
		log.Println(err)
	}
	err = p.db.Close()
	if err != nil {
		log.Println(err)
	}
	log.Println("Close")
	p.isClosed = true
}

// makeMessage делает сообщение из данных в БД
func (p *Producer) makeMessage() ([]byte, int, error) {
	message := Message{}

	var row = p.db.QueryRow(
		`SELECT ID, msg FROM producer WHERE status IS NULL LIMIT 1`,
	)

	if err := row.Scan(&message.Id, &message.Msg); err != nil {
		if err == sql.ErrNoRows {
			return nil, -1, err
		}
		log.Fatal(err)
	}

	message.Dt = time.Now().UTC().String()

	frame, err := json.Marshal(message)
	if err != nil {
		log.Fatal(err)
	}
	return frame, message.Id, nil
}

// commitStatusData фиксирует стутус в БД отправленных данных
func (p *Producer) commitStatusData(id int) {
	_, err := p.db.Exec(
		`UPDATE producer SET status = 1 WHERE ID = $1`,
		id,
	)
	if err != nil {
		log.Println(err)
	}
}

// loopPush отправляет данные PUSH
func (p *Producer) loopPush() {
	for {
		frame, id, err := p.makeMessage()

		if err != nil {
			time.Sleep(10 * time.Millisecond)
			continue
		}

		if err = p.socket.SendMessage([][]byte{frame}); err != nil {
			time.Sleep(10 * time.Millisecond)
			log.Println(err)
			continue
		}

		p.commitStatusData(id)
		time.Sleep(time.Second)
	}
}

// Run запускает поставщика данных
func (p *Producer) Run() error {
	if p.isClosed {
		return fmt.Errorf("%s", "Producer is closed")
	}
	defer p.Close()
	go p.loopPush()
	<-p.ctx.Done()
	return nil
}
